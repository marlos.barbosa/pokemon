import { getNameAndURL } from "../funcoes/getNamesAndURL.js"
import { getName } from "../funcoes/getNamesAndURL.js"
import { newNamePokemon } from "../funcoes/switchPokemonName.js"

const renamePokemon = async(type,oldName,newName) =>{
    const pokeData = await getNameAndURL(type);
    const pokenames = await getName(type);
    await newNamePokemon(pokeData,pokenames,oldName,newName)

}

const oldPokemonName = "bulbasaur"
const newPokemonName = "kobra"
renamePokemon("poison",oldPokemonName,newPokemonName);