from funcoespy.collect_data import search_name_and_URL
from funcoespy.collect_data import search_name
from funcoespy.pokemon_operations import pokeremove

to_remove_pokemon = str(input("Qual pokemon deve ser removido?"))
type_of_pokemon = str(input("Pokemon type:"))
pokemon_data = search_name_and_URL(type_of_pokemon)
pokemon_name = search_name(type_of_pokemon)
pokeremove(pokemon_data,pokemon_name,to_remove_pokemon)