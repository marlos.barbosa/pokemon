from funcoespy.collect_data import search_name
from funcoespy.collect_data import search_URL

interest_pokemon = str(input("Pokemon de interesse:"))
type_of_pokemon = str(input("Pokemon type:"))

pokemon_URL = search_URL(type_of_pokemon)
pokemon_name = search_name(type_of_pokemon)

for i in range(0,len(pokemon_URL)):
    if pokemon_name[i]==interest_pokemon:
        array = pokemon_URL[i].split("/")
        print(f'{pokemon_URL[i]},{array[6]},{i}')