import { addNewPokemon } from "../funcoes/pokeOperations.js";
import { getName } from "../funcoes/getNamesAndURL.js";
import { getNameAndURL } from "../funcoes/getNamesAndURL.js";

const addPokemon = async (key1,key2)=>{
    const pokeData = await getNameAndURL(key1);
    const pokenames = await getName(key1);
    await addNewPokemon(pokeData,pokenames,key2)
}

const novoPokemon = {
    name:'snake',
    url: 'https://pokeapi.co/api/v2/pokemon/secret/'
}
addPokemon("poison",novoPokemon);