export const sortPokemon = async(pokedados,sortCriteria)=>{
    if (sortCriteria === "crescente"){
        console.log(pokedados.sort((a, b) => a.name.localeCompare(b.name)))
    }else if (sortCriteria === "decrescente"){
        console.log(pokedados.sort((a, b) => b.name.localeCompare(a.name)))
    }
}