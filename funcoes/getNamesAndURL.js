import fetch from "node-fetch"

export const  getNameAndURL = async (key)=>{
    function getURL(key){
        const baseURL = "https://pokeapi.co/api/v2/type/";
        return `${baseURL}${key}`
    }
    const response = await fetch(getURL(key));
    const pokemon = await response.json();
    const pokedata = pokemon.pokemon.map(x=>x.pokemon)
    return pokedata;
}

export const getName = async (key)=>{
    function getURL(key){
        const baseURL = "https://pokeapi.co/api/v2/type/";
        return `${baseURL}${key}`
    }
    const response = await fetch(getURL(key));
    const pokemon = await response.json();
    const pokedata = pokemon.pokemon.map(x=>x.pokemon)
    const pokenames = pokedata.map(x=>x.name)
    return pokenames;
}

export const getURLOnly = async (key)=>{
    function getURL(key){
        const baseURL = "https://pokeapi.co/api/v2/type/";
        return `${baseURL}${key}`
    }
    const response = await fetch(getURL(key));
    const pokemon = await response.json();
    const pokedata = pokemon.pokemon.map(x=>x.pokemon)
    const pokeURL = pokedata.map(x=>x.url)
    return pokeURL;
}