export const itensInCommon = async(pokedados1,pokedados2)=>{
    console.log(pokedados1.filter(o => pokedados2.some(({name,url}) => o.name === name && o.url === url)))
}
export const mergeLists = async (pokedados1,pokedados2)=>{
    let dados = new Set(pokedados1.map(d=>d.name))
    let merged = [...pokedados1, ...pokedados2.filter(d => !dados.has(d.name))]
    console.log(merged)
}
