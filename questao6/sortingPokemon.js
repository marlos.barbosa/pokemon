import { getNameAndURL } from "../funcoes/getNamesAndURL.js";
import { sortPokemon } from "../funcoes/pokemonInOrder.js";

const reordering = async(type,criteria)=>{
    const pokeData = await getNameAndURL(type);
    await sortPokemon(pokeData,criteria)
}


const criteria = "decrescente"
reordering("poison",criteria);