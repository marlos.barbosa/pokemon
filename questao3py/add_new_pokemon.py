from funcoespy.collect_data import search_name_and_URL
from funcoespy.collect_data import search_name
from funcoespy.pokemon_operations import pokeadd

new_pokemon = {}
name_new_pokemon = str(input("Name:"))
url_new_pokemon = str(input("Url number:"))
new_pokemon["name"]= name_new_pokemon
new_pokemon["url"]= (f'https://pokeapi.co/api/v2/pokemon/{url_new_pokemon}/')
type_of_pokemon = str(input("Pokemon type:"))
pokemon_data=search_name_and_URL(type_of_pokemon)
pokemon_name=search_name(type_of_pokemon)

pokeadd(pokemon_data,pokemon_name,new_pokemon)
