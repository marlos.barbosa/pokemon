import {getName, getURLOnly} from "../funcoes/getNamesAndURL.js"


const presentURLNumberIndex = async(key1,key2)=>{
    const pokenames = await getName(key1);
    const pokeURL = await getURLOnly(key1);
    const interestPokemon = key2
    for (let i = 0; i<pokenames.length; i++){
        if(pokenames[i]==interestPokemon){
            const array = pokeURL[i].split("/")
            console.log(`${pokeURL[i]}, ${array[6]}, ${i}`)
        }
    }
}

presentURLNumberIndex("poison","weedle");