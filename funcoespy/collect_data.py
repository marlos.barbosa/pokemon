import requests

def search_name_and_URL(key):
    response = requests.get("https://pokeapi.co/api/v2/type/"+key)
    data = response.json()
    pokemon_data_and_slot = data['pokemon']
    pokemon_data=[]
    for i in pokemon_data_and_slot:
        pokemon_data.append(i['pokemon'])
    return pokemon_data

def search_name(key):
    response = requests.get("https://pokeapi.co/api/v2/type/"+key)
    data = response.json()
    pokemon_data_and_slot = data['pokemon']
    pokemon_data=[]
    for i in pokemon_data_and_slot:
        pokemon_data.append(i['pokemon']['name'])
    return pokemon_data

def search_URL(key):
    response = requests.get("https://pokeapi.co/api/v2/type/"+key)
    data = response.json()
    pokemon_data_and_slot = data['pokemon']
    pokemon_data=[]
    for i in pokemon_data_and_slot:
        pokemon_data.append(i['pokemon']['url'])
    return pokemon_data