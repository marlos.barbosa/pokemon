from funcoespy.collect_data import search_name_and_URL
from funcoespy.pokemon_sorter import pokesort


type_of_pokemon = str(input("Pokemon type:"))
sort_criteria = str(input("Critério de ordenação: (crescente/decrescente)"))
pokemon_data = search_name_and_URL(type_of_pokemon)

pokesort(pokemon_data,sort_criteria)