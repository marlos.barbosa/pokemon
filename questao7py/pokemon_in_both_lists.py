from funcoespy.collect_data import search_name_and_URL
from funcoespy.using_two_lists import pokemon_in_both_lists

type_of_pokemon1 = str(input("First pokemon type:"))
type_of_pokemon2 = str(input("Second pokemon type:"))
pokemon_data1 = search_name_and_URL(type_of_pokemon1)
pokemon_data2 = search_name_and_URL(type_of_pokemon2)

pokemon_in_both_lists(pokemon_data1,pokemon_data2)