import { getNameAndURL } from "../funcoes/getNamesAndURL.js";
import { getName } from "../funcoes/getNamesAndURL.js";
import { removePokemon } from "../funcoes/pokeOperations.js";

const deletePokemon = async(type,oldPokemon) =>{
    const pokeData = await getNameAndURL(type);
    const pokenames = await getName(type);
    await removePokemon(pokeData,pokenames,oldPokemon)

}

const oldPokemonName = "bulbasaur"
deletePokemon("poison",oldPokemonName);