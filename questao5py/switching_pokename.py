from funcoespy.collect_data import search_name_and_URL
from funcoespy.collect_data import search_name
from funcoespy.pokemon_operations import pokeswitch

old_name = str(input("Nome antigo do pokemon:"))
new_name = str(input("Novo nome do pokemon:"))
type_of_pokemon = str(input("Pokemon type:"))
pokemon_data = search_name_and_URL(type_of_pokemon)
pokemon_name = search_name(type_of_pokemon)
pokeswitch(pokemon_data,pokemon_name,old_name,new_name)