import { getNameAndURL } from "../funcoes/getNamesAndURL.js";
import { itensInCommon } from "../funcoes/twoPokemonsLists.js";


const elementsInCommon = async(type1,type2)=>{
    const pokeData1 = await getNameAndURL(type1);
    const pokeData2 = await getNameAndURL(type2);
    await itensInCommon(pokeData1,pokeData2)

}

elementsInCommon("fire","flying");