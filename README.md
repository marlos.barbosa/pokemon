# pokemon

Atividade 3

## Como executar os códigos?

 # Para Python
Para executar os programas em python, é necessário ir até a pasta pokemon e passar um comando do tipo:
python -m questao(i)py.(nome_da_função)
Em que:
    i indicará o número da questão a ser resolvida
    nome_da_função indicará o nome do arquivo que resolve tal problema

Exemplo (Resolvendo questão 1):
Devo estar em:
    C:\Users\marlo\Documents\pokemon
E executar o comando:
    python -m questao1py.getData
    
 # Para Javascript
 Antes de tudo, é necessário ir até a pasta funcoes e executar o comando:
    npm install --save node-fetch
 Após isso, é necessário ir até a pasta com cada resolução e executar o comando:
    npm init
 Nos as arquivos pockage.json criados nas pastas, deve ser adicionado o seguinte:
    "type":"module"
 Após isso, para executar os programas em javascript, é necessário ir até a pasta
 em que a resolução da questão está localizada e executar um comando do tipo:
 node nome_da_função

